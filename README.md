**Trainer's Guide**

**Materials Needed:**

* A GitLab instance with master branch protection turned off
* A user for everyone in the training. If they are using a test instance we set up, you can make user1, user2, user3, etc. If they are using their own instance, we just need to make sure we have a user on the system.
* Training Slide Deck (currently [here](https://docs.google.com/presentation/d/1G5KAZNvZ_iRpz39jf1MvMzKyaFYclZ0LItHoeinlOpU/edit#slide=id.g18a601cca0_0_18), reveal version, will be made _soon_)
* Trainer Guide (You are reading it!)
* A Pro Zoom account so you can record
* An example of a repo with feature branching

**How this training works:**

There are two sessions, 90 minutes each, and each session has an end goal that should be accomplished.  Session 1 is built around re-enforcing how the UI works, and then paralleling that in the command line. Session 2 focuses on Merge Conflicts in the UI and command line and Git/Hub/Lab Flow.

**Session 1 (90 minutes)**

_Part 1: The UI (45-55 minutes)_

The training starts by having all users log in to the instance, the instructor makes a project and a readme.md via the UI. Then the instructor adds a file to the repo that is `trainers_name.md`and adds a sentence about themselves. Next, the users will do the same. As the instructor, you can watch the changes come in and navigate to a few to reenforce how the UI works. At this point, you can explain that we've all committed to master, and that their is another workflow, branching. 

Use the UI to make a branch and push a new sentence to `trainer_name.md` with your changes to it, and then make the merge request. Instruct them to do the same. It's a great moment to talk about merge requests and discussions. Find a few merge requests, make a comment and ask the students to respond. BEFORE YOU MERGE, make a point to explain how master doesn't have the changes until the merge request is accepted. Also a fun thing to note is that a _closed_ merged request does not necessarily mean _merged_ code.

After the changes have been merged to master, we can take a moment to step back and talk about git concepts. Up until this point, the focus should be GitLab UI and now we can dive into the _git_ ideas (DVCS, commits, DR ,etc). At this point, it's time to break, and you can let them know that we'll be moving to the command line after the  break and they should have GitBash (if they are on Windows) and if they don't, now is the time to install.

_5/10 minute break_

_Part 2: Command Line (45 minutes)_

At this point, it's important to re-enforce how we've been working on the server the whole time, but the power of git comes from working locally (reenforcing the points we covered in the git concepts section). We'll pull the code project down, and show them that it makes a directory locally, and we can see all of the files we made. Instruct them to do the same. Dive into the basic git commands (and git config), and we'll make a change to our files, and  You'll push that change up. They can do the same. Go to the server, see the changes as them come in, and explain what happened. Now, you make a change on your branch, push it up, and make the MR. Have them do the same. Reenforce that changes will _not_ go on the server, until they push them. Master won't have changes from their branch until we merge them in. Take the last 5 minutes or so to recap that what we did today, editing code in the UI, editing code remotely, and getting local code remote.  This is the end of session one, and they have all had 4 instances of using GitLab with Git. :)

**Session 2 (90 minutes)**

_Part 1: The UI (30 minutes)_

Start Session 2 with a recap of what we covered in the previous session, and how today is going to get more difficult. We are gonna talk about when things go wrong. Make a contrived merge conflict by making a new project, committing Line 1, Line 2 to master. Make a branch and commit line 3, line 4 to that branch. Go back to master,  underneath line 1 and line 2 and add line 5, line 6. So now we'll have a conflict between 3/4 - 5/6. Make the Merge request, and you can resolve it via the UI. Once it's resolved, we can move to the command line portion.

_Part 2: The Command Line (30 minutes)_

_Part 3:  Git/Hub/Lab Flow (20 minutes)_

_to be continued_